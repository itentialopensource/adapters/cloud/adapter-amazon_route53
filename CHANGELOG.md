
## 0.4.8 [11-12-2024]

* more auth changes

See merge request itentialopensource/adapters/adapter-amazon_route53!25

---

## 0.4.7 [10-15-2024]

* Changes made at 2024.10.14_20:10PM

See merge request itentialopensource/adapters/adapter-amazon_route53!24

---

## 0.4.6 [09-30-2024]

* update auth docs

See merge request itentialopensource/adapters/adapter-amazon_route53!22

---

## 0.4.5 [09-12-2024]

* add properties for sts

See merge request itentialopensource/adapters/adapter-amazon_route53!21

---

## 0.4.4 [08-22-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-amazon_route53!20

---

## 0.4.3 [08-14-2024]

* Changes made at 2024.08.14_18:18PM

See merge request itentialopensource/adapters/adapter-amazon_route53!19

---

## 0.4.2 [08-06-2024]

* Changes made at 2024.08.06_19:32PM

See merge request itentialopensource/adapters/adapter-amazon_route53!18

---

## 0.4.1 [08-05-2024]

* Changes made at 2024.08.05_19:17PM

See merge request itentialopensource/adapters/adapter-amazon_route53!17

---

## 0.4.0 [07-08-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-amazon_route53!16

---

## 0.3.9 [03-28-2024]

* Changes made at 2024.03.28_13:14PM

See merge request itentialopensource/adapters/cloud/adapter-amazon_route53!15

---

## 0.3.8 [03-21-2024]

* Changes made at 2024.03.21_13:49PM

See merge request itentialopensource/adapters/cloud/adapter-amazon_route53!14

---

## 0.3.7 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/cloud/adapter-amazon_route53!13

---

## 0.3.6 [03-13-2024]

* Changes made at 2024.03.13_11:45AM

See merge request itentialopensource/adapters/cloud/adapter-amazon_route53!12

---

## 0.3.5 [03-11-2024]

* Changes made at 2024.03.11_15:30PM

See merge request itentialopensource/adapters/cloud/adapter-amazon_route53!11

---

## 0.3.4 [02-28-2024]

* Changes made at 2024.02.28_11:42AM

See merge request itentialopensource/adapters/cloud/adapter-amazon_route53!10

---

## 0.3.3 [01-27-2024]

* make changes non breaking

See merge request itentialopensource/adapters/cloud/adapter-amazon_route53!9

---

## 0.3.2 [12-26-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-amazon_route53!8

---

## 0.3.1 [12-14-2023]

* Remediation Merge Request

See merge request itentialopensource/adapters/cloud/adapter-amazon_route53!7

---

## 0.3.0 [11-10-2023]

* Minor/2023 auto migration

See merge request itentialopensource/adapters/cloud/adapter-amazon_route53!6

---

## 0.2.2 [07-14-2022]

* patch/Auth - Auth changes

See merge request itentialopensource/adapters/cloud/adapter-amazon_route53!3

---

## 0.2.1 [06-24-2022]

- Auth Changes for Headers and Streamline

See merge request itentialopensource/adapters/cloud/adapter-amazon_route53!2

---

## 0.2.0 [05-15-2022]

- Changes for STS Auth, IAM and Migration
  - Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
  - Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
  - Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
  - Add scripts for easier authentication, removing hooks, etc
  - Script changes (install script as well as database changes in other scripts)
  - Double # of path vars on generic call
  - Update versions of foundation (e.g. adapter-utils)
  - Update npm publish so it supports https
  - Update dependencies
  - Add preinstall for minimist
  - Fix new lint issues that came from eslint dependency change
  - Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
  - Add the adapter type in the package.json
  - Add AdapterInfo.js script
  - Add json-query dependency
  - Add the propertiesDecorators.json for product encryption
  - Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
  - Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
  - Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
  - Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
  - Add AdapterInfo.json
  - Add systemName for documentation

See merge request itentialopensource/adapters/cloud/adapter-amazon_route53!1

---

## 0.1.1 [03-06-2022]

- Initial Commit

See commit ff30dcd

---
