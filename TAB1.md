# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Amazon_route53 System. The API that was used to build the adapter for Amazon_route53 is usually available in the report directory of this adapter. The adapter utilizes the Amazon_route53 API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The AWS route53 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AWS Route53. With this adapter you have the ability to perform operations such as:

- Configure and Manage DNS. 
- Get, or Create Traffic Policy
- Get, Create, or Delete Hosted Zone
- List Hosted Zones

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
