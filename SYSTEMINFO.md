# Amazon Route53

Vendor: Amazon Web Services
Homepage: https://aws.amazon.com/

Product: Route53
Product Page: https://aws.amazon.com/route53/

## Introduction
We classify AWS Route53 into the Cloud domain as Route53 provides capabilities in managing and orchestrating domain name system sercices within cloud environments.

"Route53 offers Domain Name Services (DNS)" 
"Route53 allows for customizing Domain Name Services to improve application availability" 

## Why Integrate
The AWS route53 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AWS Route53. With this adapter you have the ability to perform operations such as:

- Configure and Manage DNS. 
- Get, or Create Traffic Policy
- Get, Create, or Delete Hosted Zone
- List Hosted Zones

## Additional Product Documentation
The [API documents for AWS Route53](https://docs.aws.amazon.com/Route53/latest/APIReference/Welcome.html)
